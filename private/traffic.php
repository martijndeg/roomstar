<?php

function traffic_info() {
    
    $traffic_info = [];
    
    $feed = file_get_contents('https://www.anwb.nl/feeds/gethf');
    
    $entries = json_decode($feed)->roadEntries;
    
    $watch_roads = [
        'A2', 'A15', 'A270', 'A325', 'A50', 'A58', 'A67', 'A73', 'N2', 'N270', 'N277',
    ];
    
    foreach($entries as $entry) {
//        if(in_array($entry->road, $watch_roads)) {
        if($entry->roadType == 'aWegen' || $entry->roadType == 'nWegen') {
            $traffic_info[] = $entry;
        }
//        }
    }
    
    return $traffic_info;
    
}

