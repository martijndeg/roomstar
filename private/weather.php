<?php

    function get_weather($view)
    {
        $weather_url = 'https://api.buienradar.nl/data/public/2.0/jsonfeed';
    
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $weather_url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        $result = json_decode(curl_exec($ch));
        $weather = $result->actual->stationmeasurements[6];
        $forecasts = $result->forecast->fivedayforecast;
        curl_close($ch);
    
    
        $main = '';
        $today = '';
    
        $main .= '<h1>' . $weather->regio . '</h1>';
        $main .= '<h3><span><i class="wi wi-thermometer-internal"></i> ' . $weather->temperature . ' <i class="wi wi-celsius"></i></span></h3>';
        $main .= '<small id="weather_description">' . $weather->weatherdescription . '</small>';
    
        $today .= '<article class="today-summary"><header><h2><div id="clock"></div><br>' . strftime('%e %B %G', strtotime($weather->timestamp)) . '</h2>';
        $today .= '<p>Luchtvochtigheid ' . $weather->humidity . '&percnt; | Zicht: ' . number_format($weather->visibility / 1000, 1) . ' KM | <i class="wi wi-wind"></i> ' . $weather->winddirection . ' ' . $weather->windspeed . '</p></header></article>';
    
        $i = 0;
        foreach($forecasts as $forecast) { if($i < 3) {
            $today .= '<article class="forecast-container"><header>';
            $today .= '<img class="forecast-icon" src="' . $forecast->iconurl . '">';
            $today .= '<p><i class="wi wi-thermometer-internal"></i> ' . $forecast->mintemperature . '<i class="wi wi-celsius"></i> / ' . $forecast->maxtemperature . '<i class="wi wi-celsius"></i></p></header>';
            $today .= '<p>' . strftime('%e %B %G', strtotime($forecast->day)) . '</p></article>';
        } $i++; }
        
        if($view == 'main') {
            return $main;
        } else if($view == 'today') {
            return $today;
        }
    }



    function weather_icon($code, $astronomy)
    {
        if(time() > strtotime($astronomy->sunrise) && time() < strtotime($astronomy->sunset))
        {
            $day_night = 'day';
        }
        else
        {
            $day_night = 'night';
        }
        
        $code = (int) $code;
        
        switch($code) {
            case 0:
                $icon = 'tornado';
                break;
            case 1:
            case 2:
            case 3:
            case 4:
                $icon = $day_night.'-thunderstorm';
                break;
            case 5:
                $icon = $day_night.'-snow';
                break;
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
                $icon = $day_night.'-sleet';
                break;
            case 15:
                $icon = $day_night.'-snow-wind';
                break;
            case 16:
                $icon = $day_night.'-snow';
                break;
            case 17:
                $icon = $day_night.'-hail';
                break;
            case 18:
                $icon = $day_night.'-sleet';
                break;
            case 19:
                $icon = 'dust';
                break;
            case 20:
                $icon = $day_night.'-fog';
                break;
            case 21:
                $icon = 'day-haze';
                break;
            case 22:
                $icon = 'smoke';
                break;
            case 23:
            case 24:
                $icon = 'windy';
                break;
            case 25:
                $icon = 'snowflake-cold';
                break;
            case 26:
                $icon = $day_night.'-cloudy';
                break;
            case 27:
            case 29:
                $icon = 'night-cloudy';
                break;
            case 28:
            case 30:
                $icon = 'day-cloudy';
                break;
            case 31:
            case 33:
                $icon = 'night-clear';
                break;
            case 32:
            case 34:
                $icon = 'day-sunny';
                break;
            case 35:
                $icon = $day_night.'-rain-mix';
                break;
            case 36:
                $icon = 'hot';
                break;
            case 37:
            case 38:
            case 39:
                $icon = $day_night.'-thunderstorm';
                break;
            case 40:
                $icon = $day_night.'-showers';
                break;
            case 41:
            case 43:
                $icon = $day_night.'-snow-wind';
                break;
            case 42:
                $icon = $day_night.'-snow';
                break;
            case 44:
                $icon = $day_night.'-cloudy';
                break;
            case 45:
                $icon = 'thunderstorm';
                break;
            case 46:
                $icon = $day_night.'-snow-wind';
                break;
            case 47:
                $icon = $day_night.'-thunderstorm';
                break;
            default:
                $icon = 'sunny';
                break;
        }
        return ' wi-'.$icon;
    }