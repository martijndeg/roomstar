<?php
/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 29/12/2018
 * Time: 13:49
 */

class Notification
{
    protected $api_url = 'https://api.pushover.net/1/messages.json';
    protected $token = 'a2sgq8xebva16s2asvij3f6oofss6n';
    protected $user_key = 'u5p1excwwh78b1ayi5ezihvh56iaqp';
    
    protected $message;
    protected $url = 'https://www.google.com';
    
    public function __construct(String $message, String $url = null)
    {
        $this->message = $message;
        
        if($url !== null) {
            $this->url = $url;
        }
    
        curl_setopt_array($ch = curl_init(), array(
            CURLOPT_URL => $this->api_url,
            CURLOPT_POSTFIELDS => array(
                'token' => $this->token,
                'user' => $this->user_key,
                'message' => $this->message,
                'url' => $this->url
            ),
            CURLOPT_SAFE_UPLOAD => true,
            CURLOPT_RETURNTRANSFER => true,
        ));
        curl_exec($ch);
        curl_close($ch);
    }
}