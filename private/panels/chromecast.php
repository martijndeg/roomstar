<?php
if($chromecast_image->getState() !== 'UNDEF') {
    $chromecast_image_url = str_replace('250x250', '1000x1000', $chromecast_image->getState());
} else {
    $chromecast_image_url = false;
}
?>
<div id="player_background"<?= (!$chromecast_image_url) ? '' : ' style="background: url(' . $chromecast_image_url . '); background-size: cover; background-position: center;"'?>></div>
<div class="tile tile-status">
    <?php if($chromecast->statusInfo->status === "ONLINE") { ?>
        <div id="media_status">
            <i class="ti-control-play <?= ($chromecast_media_idling->getState() == 'OFF') ? 'on' : 'off' ?>"></i>
            <div id="app_state">
                <?= ($chromecast_media_title->getState() !== 'UNDEF') ? $chromecast_media_title->getState() : '' ?> | <small><?= ($chromecast_status->getState() !== 'UNDEF') ? $chromecast_status->getState() : '' ?></small>
            </div>
            <div id="app_progress">
                <div id="progress_time">
                    <span class="progress-bar" style="width: <?= ($chromecast_total_time->getState() !== 'UNDEF') ? 100 - (($chromecast_current_time->getState() / $chromecast_total_time->getState()) * 100) : 0 ?>%"></span>
                </div>
                <small id="volume_container" class="hidden">Volume <span class="integer"><?= $chromecast_volume->getState(); ?></span>&percnt;</small>
                <small id="time_container"><span class="current"><?= seconds2human($chromecast_current_time->getState()) ?></span> / <span class="total"><?= seconds2human($chromecast_total_time->getState()) ?></span></small>
            </div>
        </div>
        <script>console.log(<?= json_encode($chromecast_current_time->getInfo()) ?>);</script>
    <?php } else { ?>
        <h3><i class="ti-power-off off"></i></h3>
    <?php } ?>

    <?php if($chromecast_image->getState() !== 'UNDEF' && false == true) { ?>
        <div class="media-image">
            <img src="<?= $chromecast_image->getState() ?>" class="image">
        </div>
    <?php } ?>
    <div class="media-control">
        <a href="#" id="volume_down">
            <i class="ti-minus"></i>
        </a>
    </div>
    <div class="media-control">
        <a href="#" id="volume_up">
            <i class="ti-plus"></i>
        </a>
    </div>
    <div class="media-control">
        <a href="#" id="play">
            <div class="button-slider <?= ($chromecast_control->getState() == 'PAUSE') ? 'top' : 'bottom' ?>">
                <div class="button-top">
                    <i class="ti-control-play"></i>
                </div>
                <div class="button-bottom">
                    <i class="ti-control-pause"></i>
                </div>
            </div>
        </a>
    </div>
    <div class="media-control">
        <a href="#" id="mute">
            <i class="ti-volume<?= ($chromecast_mute->getState() == 'ON') ? ' red' : '' ?>"></i>
        </a>
    </div>
</div>
