<div class="traffic-container">
    
    <?php
    if(!empty(traffic_info())) {
        foreach(traffic_info() as $traffic_info) {
            ?>
            
            <div class="tile tile-traffic">
                <div class="road <?= $traffic_info->roadType ?>">
                    <div class="road-sign">
                        <?= $traffic_info->road ?>
                    </div>
                    
                    <?php
                    if (isset($traffic_info->events->trafficJams)) {
                        foreach ($traffic_info->events->trafficJams as $trafficJam) {
                            ?>
                            <a href="#" class="send-traffic-info traffic-jam">
                                <input type="hidden" class="from-lat" value="<?= $trafficJam->fromLoc->lat ?>">
                                <input type="hidden" class="from-long" value="<?= $trafficJam->fromLoc->lon ?>">
                                <input type="hidden" class="to-lat" value="<?= $trafficJam->toLoc->lat ?>">
                                <input type="hidden" class="to-long" value="<?= $trafficJam->toLoc->lon ?>">
                                <div class="description">
                                    <span class="location"><?= $trafficJam->from ?> - <?= $trafficJam->to ?></span>
                                    <div class="reason"><?= $trafficJam->description; ?></div>
                                </div>
                            </a>
                            
                            <?php
                        }
                    }
                    ?>
                    
                    <?php
                    if (isset($traffic_info->events->roadWorks)) {
                        foreach ($traffic_info->events->roadWorks as $roadwork) {
                            ?>
                            
                            <a href="#" class="send-traffic-info roadworks">
                                <input type="hidden" class="from-lat" value="<?= $roadwork->fromLoc->lat ?>">
                                <input type="hidden" class="from-long" value="<?= $roadwork->fromLoc->lon ?>">
                                <input type="hidden" class="to-lat" value="<?= $roadwork->toLoc->lat ?>">
                                <input type="hidden" class="to-long" value="<?= $roadwork->toLoc->lon ?>">
                                <div class="description">
                                    <span class="location"><?= $roadwork->from ?> - <?= $roadwork->to ?></span>
                                    <div class="reason"><?= $roadwork->description; ?></div>
                                </div>
                            </a>
                            
                            <?php
                        }
                    }
                    ?>
                    
                    <?php
                    if (isset($traffic_info->events->radars)) {
                        foreach ($traffic_info->events->radars as $radar) {
                            ?>
                            
                            <a href="#" class="send-traffic-info radar">
                                <input type="hidden" class="from-lat" value="<?= $radar->fromLoc->lat ?>">
                                <input type="hidden" class="from-long" value="<?= $radar->fromLoc->lon ?>">
                                <input type="hidden" class="to-lat" value="<?= $radar->toLoc->lat ?>">
                                <input type="hidden" class="to-long" value="<?= $radar->toLoc->lon ?>">
                                <div class="description">
                                    <span class="location"><?= $radar->from ?> - <?= $radar->to ?></span>
                                    <div class="reason"><?= $radar->description; ?></div>
                                </div>
                            </a>
                            
                            <?php
                        }
                    }
                    ?>
                
                </div>
            </div>
            
            <?php
        }
    } else {
        echo '<p>Geen verkeersinformatie</p>';
    }
    ?>

</div>