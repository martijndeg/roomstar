<div class="lighting-container">
    <div class="tile">
        <i class="icon-comfy-couch"></i>
        <small class="light-indicator"><i class="ti-light-bulb on"></i></small>
    </div>
    <div class="tile">
        <i class="icon-desk"></i>
        <small class="light-indicator"><i class="ti-light-bulb off"></i></small>
    </div>
</div>
<div class="lighting-container">
    <div class="tile">
        <i class="icon-desk-lamp"></i>
        <small class="light-indicator"><i class="ti-light-bulb off"></i></small>
    </div>
    <div class="tile">
        <i class="icon-bed"></i>
        <small class="light-indicator"><i class="ti-light-bulb on"></i></small>
    </div>
</div>