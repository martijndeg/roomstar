<div class="settings-container">
    <div class="tile tile-mode">
        <i class="wi wi-day-sunny"></i>
        <div id="day_start_time" class="time">
            <div class="hours-container">
                <input type="number" min="00" max="23" step="01" class="hours" value="07">
                <a href="#" class="subtract-hour"><i class="ti-minus"></i></a>
                <a href="#" class="add-hour"><i class="ti-plus"></i></a>
            </div>
            <div class="minutes-container">
                <input type="number" min="00" max="59" step="01" class="minutes" value="30">
                <a href="#" class="subtract-minute"><i class="ti-minus"></i></a>
                <a href="#" class="add-minute"><i class="ti-plus"></i></a>
            </div>
        </div>
    </div>
    <div class="tile tile-mode">
        <i class="wi wi-moonrise"></i>
        <div id="evening_start_time" class="time">
            <div class="hours-container">
                <input type="number" min="0" max="23" step="01" class="hours" value="18">
                <a href="#" class="subtract-hour"><i class="ti-minus"></i></a>
                <a href="#" class="add-hour"><i class="ti-plus"></i></a>
            </div>
            <div class="minutes-container">
                <input type="number" min="00" max="59" step="01" class="minutes" value="30">
                <a href="#" class="subtract-minute"><i class="ti-minus"></i></a>
                <a href="#" class="add-minute"><i class="ti-plus"></i></a>
            </div>
        </div>
    </div>
    <div class="tile tile-mode">
        <i class="wi wi-night-clear"></i>
        <div id="night_start_time" class="time">
            <div class="hours-container">
                <input type="number" min="0" max="23" step="01" class="hours" value="00">
                <a href="#" class="subtract-hour"><i class="ti-minus"></i></a>
                <a href="#" class="add-hour"><i class="ti-plus"></i></a>
            </div>
            <div class="minutes-container">
                <input type="number" min="00" max="59" step="01" class="minutes" value="00">
                <a href="#" class="subtract-minute"><i class="ti-minus"></i></a>
                <a href="#" class="add-minute"><i class="ti-plus"></i></a>
            </div>
        </div>
    </div>
</div>