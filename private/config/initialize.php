<?php
setlocale(LC_ALL, 'nl_NL');
error_reporting(0);
ini_set('display_errors', '1');

//$BASE_URL = "http://query.yahooapis.com/v1/public/yql";
//$yql_query = 'select * from weather.forecast where woeid in (select woeid from geo.places(1) where text="eindhoven, nl") and u = "c"';
//$yql_query_url = $BASE_URL . "?q=" . urlencode($yql_query) . "&format=json";

$url = "http://192.168.178.11:8080/rest/things/chromecast%3Achromecast%3A87de5b3a6c2b4132f7bd5ded60f21bb5";

$chromecast = file_get_contents($url);

$chromecast = json_decode($chromecast);

//$json = file_get_contents($yql_query_url);
//$phpObj = json_decode($json);

//var_dump($yql_query_url);

function seconds2human($ss) {
    $s = $ss % 60;
    $m = floor(($ss % 3600) / 60);
    $h = floor(($ss % 86400) / 3600);
    $d = floor(($ss % 2592000) / 86400);
    $M = floor($ss / 2592000);
    
    $seconds = (strlen($s) < 2) ? '0' . $s : $s;
    $minutes = ((strlen($m) < 2) ? '0' . $m : $m) . ':';
    $hours = ($h < 1) ? '' : $h . ':';
    
    $time = $hours.$minutes.$seconds;
    
    return $time;
}

include $include_path . 'class/Item.php';
include $include_path . 'notifications/Notification.php';
include $include_path . 'weather.php';
include $include_path . 'rss.php';
include $include_path . 'traffic.php';

if(isset($_POST['get_news'])) {
    getFeed('http://rss.cnn.com/rss/edition.rss');
    exit();
}

if(isset($_POST['get_weather'])) {
    $view = $_POST['type'];
    
    echo get_weather($view);
    exit();
}

$chromecast_control = new Item('LivingRoomChromecastControl');
$chromecast_volume = new Item('LivingRoomChromecastVolume');
$chromecast_mute = new Item('LivingRoomChromecastMute');
$chromecast_status = new Item('Chromecast_AppStatus');
$chromecast_current_time = new Item('LivingRoomChromecastCurrentTime');
$chromecast_total_time = new Item('LivingRoomChromecastTotalTime');
$chromecast_media_title = new Item('LivingRoomChromecastMediaTitle');
$chromecast_media_idling = new Item('LivingRoomChromecastIdling');
$chromecast_image = new Item('LivingRoomChromecastImage');


$chromecast_season = new Item('LivingRoomChromecastSeason');
$chromecast_episode = new Item('LivingRoomChromecastEpisode');

function get_chromecast_times() {
    global $chromecast_current_time;
    global $chromecast_total_time;
    global $chromecast_control;
    
    if($chromecast_total_time->getState() !== 'UNDEF') {
        $current = seconds2human($chromecast_current_time->getState());
        $total = seconds2human($chromecast_total_time->getState());
        $play_state = $chromecast_control->getState();
        
        $time = [];
        $time['play_state'] = $play_state;
        $time['percentage'] = ($chromecast_current_time->getState() / $chromecast_total_time->getState()) * 100;
        $time['current'] = $current;
        $time['total'] = $total;
        
        echo json_encode($time);
    } else {
        echo json_encode(false);
    }
    
    
    exit();
}

if(isset($_POST['get_chromecast_times'])) {
    get_chromecast_times();
    exit();
}

if(isset($_POST['play_pause'])) {
    if($chromecast_control->getState() == 'PLAY') {
        $command = 'PAUSE';
    } else {
        $command = 'PLAY';
    }
    
    echo json_encode($chromecast_control->sendCommand($command));
    exit();
}

if(isset($_POST['stop'])) {
    $chromecast_status->sendCommand('OFF');
    exit();
}

if(isset($_POST['volume'])) {
    $current_volume = (int) $chromecast_volume->getState();
    $new_volume = 0;
    
    if($_POST['volume'] == 1) {
        $new_volume = $current_volume + 5;
    } else if($_POST['volume'] == -1) {
        $new_volume = $current_volume - 5;
    }
    
    $chromecast_volume->sendCommand($new_volume);
    
    echo json_encode($new_volume);
    exit();
}

if(isset($_POST['mute'])) {
    if($chromecast_mute->getState() == 'ON') {
        $chromecast_mute->sendCommand('OFF');
    } else {
        $chromecast_mute->sendCommand('ON');
    }
    echo json_encode(true);
    exit();
}

if(isset($_POST['notification_type'])) {
    $type = $_POST['notification_type'];
    $message = $_POST['notification_message'];
    $url = $_POST['notification_url'];
    
    $notification = new Notification($message, $url);
    echo json_encode(true);
    exit();
}
