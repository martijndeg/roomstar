<?php

function getFeed($feed_url) {
    
    $content = file_get_contents($feed_url);
    $x = new SimpleXmlElement($content);
    
    // echo '<script>console.log('.json_encode($x).');</script>';
    
    echo '<article class="rss-feed">';
    $i = 0;
    foreach($x->channel->item as $entry) {
        if($i < 12) {
            echo '<a href="#" class="send-news"><input type="hidden" class="news-url" value="' . $entry->link . '"><div class="rss-item"><h3 class="item-title">'.htmlspecialchars($entry->title).'</h3></div></a>';
            if(($i + 1) % 4 == 0) {
                echo '</article><article class="rss-feed">';
            }
        }
        $i++;
    }
    echo '</article>';
}