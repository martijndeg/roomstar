<?php

class Item
{
    protected $name;
    protected $info;
    protected $state;
    protected $url;
    
    public function __construct(String $name)
    {
        $this->name = $name;
        
        $this->url = 'http://192.168.178.11:8080/rest/items/' . $this->name;
        
        $this->info = json_decode(file_get_contents($this->url));
        $this->state = $this->info->state;
    }
    
    public function refresh()
    {
        $this->info = json_decode(file_get_contents($this->url));
        $this->state = $this->info->state;
        
        return $this->info;
    }
    
    public function sendCommand(String $command)
    {
        exec('curl -X POST --header "Content-Type: text/plain" --header "Accept: application/json" -d "'.$command.'" "' . $this->url . '"
', $output);
        return $output;
    }
    
    public function getState()
    {
        return $this->state;
    }
    
    public function getInfo()
    {
        return $this->info;
    }
}