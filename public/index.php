<?php
    $include_path = '/home/pi/roomstar/private/';
    require_once $include_path . 'config/initialize.php';
?>
<!DOCTYPE html>

<html class="show_2_2" onload="">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Roomstar</title>
    <link rel="stylesheet" href="/css/themify-icons.css" type="text/css">
    <link rel="stylesheet" href="/css/weather-icons-wind.min.css" type="text/css">
    <link rel="stylesheet" href="/css/weather-icons.min.css" type="text/css">
    <link rel="stylesheet" href="/css/furniture-icons.css" type="text/css">
    <link rel="stylesheet" href="/css/style.css" type="text/css">
</head>
<body onload="setTheme(); startTime(); idleView();">
    <div id="button_bar_top" class="button-bar">
        <a href="#" class="button" id="show_2_2"><i class="ti-home"></i></a>
    </div>
    <div id="button_bar_bottom" class="button-bar">
        <a href="#" class="button" id="show_1_2"><i class="ti-music-alt"></i></a>
        <a href="#" class="button" id="show_3_2"><i class="ti-info"></i></a>
    </div>
    <div id="button_bar_left" class="button-bar">
        <a href="#" class="button" id="show_1_1"><i class="ti-car"></i></a>
        <a href="#" class="button" id="show_2_1"><i class="ti-calendar"></i></a>
        <a href="#" class="button" id="show_3_1"><i class="ti-direction-alt"></i></a>
    </div>
    <div id="button_bar_right" class="button-bar">
        <a href="#" class="button" id="show_1_3"><i class="ti-settings"></i></a>
        <a href="#" class="button" id="show_2_3"><i class="ti-light-bulb"></i></a>
        <a href="#" class="button" id="show_3_3"><i class="ti-dashboard"></i></a>
    </div>
    <div id="view_1_1" class="canvas-view">
        <div class="panel">
            <?php include $include_path . 'panels/traffic.php'; ?>
        </div>
    </div>
    <div id="view_1_2" class="canvas-view">
        <div class="panel">
            <?php include $include_path . 'panels/chromecast.php'; ?>
        </div>
    </div>
    <div id="view_1_3" class="canvas-view">
        <div class="panel">
            <?php include $include_path . 'panels/settings.php'; ?>
        </div>
    </div>
    <div id="view_2_1" class="canvas-view">
        <div class="panel" id="today_panel">
            <?php include $include_path . 'panels/today.php'; ?>
        </div>
    </div>
    <div id="view_2_2" class="canvas-view">
        <?php include $include_path . 'panels/main.php'; ?>
    </div>
    <div id="view_2_3" class="canvas-view">
        <div class="panel">
            <?php include $include_path . 'panels/lights.php'; ?>
        </div>
    </div>
    <div id="view_3_1" class="canvas-view">
        <?php include $include_path . 'panels/map.php'; ?>
    </div>
    <div id="view_3_2" class="canvas-view">
        <div class="panel" id="news_panel">
            <?php include $include_path . 'panels/news.php'; ?>
        </div>
    </div>
    <div id="view_3_3" class="canvas-view"></div>
    <script src="/js/jquery-3.3.1.min.js" type="text/javascript"></script>
    <script src="/js/trianglify.min.js" type="text/javascript"></script>
    <script src="/js/main.js" type="text/javascript"></script>
</body>

</html>
