$('body').on('click', '.button-bar .button', function(e) {
    var show = $(this).attr('id');
    $('html').removeClass (function (index, className) {
        return (className.match (/(^|\s)show_\S+/g) || []).join(' ');
    });
    $('html').addClass(show);
});

// $(function() {
//     alert('width: ' + $( window ).width() + ' height: ' + $( window ).height());
// });

//console.log(<?php echo json_encode($chromecast_volume->getInfo()); ?>);

function idleView() {
    var timer;
    window.onload = resetTimer;
    window.onmousemove = resetTimer;
    window.onmousedown = resetTimer;  // catches touchscreen presses as well
    window.ontouchstart = resetTimer; // catches touchscreen swipes as well
    window.onclick = resetTimer;      // catches touchpad clicks as well
    window.onkeypress = resetTimer;
    window.addEventListener('scroll', resetTimer, true); // improved; see comments

    function scrollViews() {
        var show;
        var scrollable_views = ['show_2_1', 'show_2_2', 'show_3_2'];

        setTimeout(function () {
            show = scrollable_views[Math.floor((Math.random() * (scrollable_views.length - 1)) + 0)];
            $('html').removeClass (function (index, className) {
                return (className.match (/(^|\s)show_\S+/g) || []).join(' ');
            });
            $('html').addClass(show);
            scrollViews();
        }, 30000);
    }

    function resetTimer() {
        clearTimeout(timer);
        timer = setTimeout(scrollViews, 600000);  // time is in milliseconds
    }
}

function setTheme(){

    var current_time = new Date().getHours();
    var pattern;
    var canvas_width = $('html').width();
    var canvas_height = $('html').height();

    if(current_time >= 6 && current_time < 18) {
        pattern = Trianglify({
            width: canvas_width,
            height: canvas_height,
            // variance: "0.88",
            cell_size: Math.floor(Math.random() * 120) + 90,
            seed: null,
            x_colors: ['#4286f4', '#dbf4ff', '#6ac8f2', '#0f729e'],
            y_colors: ['#dbf4ff', '#4286f4', '#0f729e', '#6ac8f2']
            // x_colors: ['#bfbfbf', '#404040', '#7f7f7f', '#e5e5e5'],
            // y_colors: ['#7f7f7f', '#e5e5e5', '#bfbfbf', '#404040']
        }).png();
        $('body').addClass('day').removeClass('night');
    } else if (current_time >= 0 && current_time < 6) {
        pattern = Trianglify({
            width: canvas_width,
            height: canvas_height,
            variance: "0.88",
            cell_size: Math.floor(Math.random() * 120) + 90,
            seed: null,
            x_colors: ['#4b4f56', '#292c33', '#54575e', '#77797c'],
            y_colors: ['#54575e', '#77797c', '#4b4f56', '#292c33']
        }).png();
        $('body').addClass('night').removeClass('day');
    } else {
        pattern = Trianglify({
            width: canvas_width,
            height: canvas_height,
            variance: "0.88",
            cell_size: Math.floor(Math.random() * 120) + 90,
            seed: null,
            x_colors: ['#6d3a14', '#c17943', '#e8975a', '#c16724'],
            y_colors: ['#e8975a', '#c16724', '#6d3a14', '#c17943']
        }).png();
        $('body').addClass('night').removeClass('day');
    }

    var pattern64 = pattern.substr(pattern.indexOf('base64') + 7);

    var sheet = (function() {
        var style = document.createElement("style");

        style.appendChild(document.createTextNode(""));

        document.head.appendChild(style);

        return style.sheet;
    })();
    sheet.insertRule("html { background: transparent url(data:image/png;base64,"+pattern64+"); background-position: center; }", 0.5);
}

setTimeout(function() {
    refreshNews();
    setTheme();
    refreshWeatherMain();
    refreshWeatherToday();
    $('#traffic_info').src = $('#traffic_info').src
}, 600000);

function refreshNews() {
    $.ajax({
        type: 'POST',
        url: '/',
        data: { 'get_news': true },
        dataType: 'html',
        success: function(result) {
            $('#news_panel').empty();
            $('#news_panel').append(result);
        }
    });
}

function refreshWeatherMain() {
    $.ajax({
        type: 'POST',
        url: '/',
        data: {
            'get_weather': true,
            'type' : 'main'
        },
        dataType: 'html',
        success: function(result) {
            $('#main_panel').empty();
            $('#main_panel').append(result);
        }
    });
}

function refreshWeatherToday() {
    $.ajax({
        type: 'POST',
        url: '/',
        data: {
            'get_weather': true,
            'type' : 'today'
        },
        dataType: 'html',
        success: function(result) {
            $('#today_panel').empty();
            $('#today_panel').append(result);
        }
    });
}

function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    $('div#clock').text(h + ":" + m);
    $('h5#clock').text(h + ":" + m);

    $.ajax({
        type: "POST",
        url: "/",
        data: { 'get_chromecast_times': true },
        dataType: 'json',
        success: function (result) {
            $('#time_container .current').text(result.current);
            $('#time_container .total').text(result.total);
            $('#progress_time .progress-bar').css('width', result.percentage + '%');
            if(result.play_state == 'PAUSE') {
                $('a#play > .button-slider').removeClass('bottom').addClass('top');
            } else {
                $('a#play > .button-slider').removeClass('top').addClass('bottom');
            }
        },
        error: function(xhr) {
            console.log(xhr);
        }
    });

    var t = setTimeout(startTime, 500);
}

function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}

$('body').on('click', 'a#play', function(e) {
    e.preventDefault();

    $.ajax({
        type: 'POST',
        url: '/',
        data: { 'play_pause': true },
        dataType: 'json',
        success: function(result) {
            $('a#play').find('.button-slider').toggleClass('top').toggleClass('bottom');
        },
        error: function(xhr) {
            console.log(xhr);
        }
    });
});

$('body').on('click', 'a#stop', function(e) {
    e.preventDefault();

    $.ajax({
        type: 'POST',
        url: '/',
        data: { 'stop': true },
        dataType: 'json',
        success: function(result) {
            $('#app_state').text('');
            $('#time_container .current').text('00:00');
            $('#time_container .total').text('00:00');
            $('a#play').find('.button-slider').addClass('top').removeClass('bottom');
            $('#app_progress .progress-bar').css('width', 0);
            $('#media_status > i').toggleClass('on').toggleClass('off');
        },
        error: function(xhr) {
            console.log(xhr);
        }
    });
});

$('body').on('click', 'a#volume_up, a#volume_down', function (e) {
    e.preventDefault();
    var button = $(this);
    var volume_container = $('#volume_container');
    var command;

    if(button.is('#volume_up')) {
        command = 1;
    } else {
        command = -1;
    }

    $.ajax({
        type: 'POST',
        url: '/',
        data: { 'volume': command },
        dataType: 'json',
        success: function(result) {
            volume_container.find('.integer').text(result);
            volume_container.removeClass('hidden');
            setTimeout(function() {
                volume_container.addClass('hidden');
            }, 1000);
        },
        error: function(xhr) {
            console.log(xhr);
        }
    });
});

$('body').on('click', 'a#mute', function(e) {
    e.preventDefault();

    $.ajax({
        type: 'POST',
        url: '/',
        data: { 'mute': true },
        dataType: 'json',
        success: function(result) {
            $('a#mute').find('i').toggleClass('red');
        },
        error: function(xhr) {
            console.log(xhr);
        }
    });
});

$('body').on('click', '.send-traffic-info', function(e) {
    e.preventDefault();
    var info = $(this);
    var type;
    var from = {};
    var to = {};
    if(info.hasClass('traffic-jam')) {
        type = 'File';
    } else if(info.hasClass('roadworks')) {
        type = 'Wegwerkzaamheden';
    } else if(info.hasClass('radar')) {
        type = 'Snelheidscontrole';
    } else {
        type = 'Geen info';
    }

    from.lat = info.find('.from-lat').val();
    from.long = info.find('.from-long').val();

    to.lat = info.find('.to-lat').val();
    to.long = info.find('.to-long').val();

    var url = 'https://www.google.com/maps/dir/?api=1';
    var origin = '&origin=' + from.lat + ',' + from.long;
    var destination = '&destination=' + to.lat + ',' + to.long;
    var newUrl = url + origin + destination;
    var road = info.closest('.tile').find('.road-sign').text();
    var message = road.trim() + ' ' + info.find('.location').text().trim() + ' \n ' + info.find('.reason').text().trim();

    $.ajax({
        type: 'POST',
        url: '/',
        dataType: 'json',
        data: {
            'notification_type': type,
            'notification_url': newUrl,
            'notification_message': message
        },
        success: function(result) {
            console.log(result);
        },
        error: function(xhr) {
            console.log(xhr);
        }
    });
});

$('body').on('click', '.send-news', function(e) {
    e.preventDefault();
    var item = $(this);
    var url = item.find('.news-url').val();
    var title = item.find('.item-title').text().trim();

    $.ajax({
        type: 'POST',
        url: '/',
        dataType: 'json',
        data: {
            'notification_type': 'News',
            'notification_url': url,
            'notification_message': title
        },
        success: function(result) {
            console.log(result);
        },
        error: function(xhr) {
            console.log(xhr);
        }
    });
});

$('body').on('click', '.add-hour', function(e) {
    e.preventDefault();
    var hours = $(this).closest('div').find('input.hours');
    var value = parseInt(hours.val()) + 1;
    if(value < 24 && value > -1) {
        if(value < 10) {
            value = '0' + value;
        }
        hours.val(value);
    }
});

$('body').on('click', '.subtract-hour', function(e) {
    e.preventDefault();
    var hours = $(this).closest('div').find('input.hours');
    var value = parseInt(hours.val()) - 1;
    if(value < 24 && value > -1) {
        if(value < 10) {
            value = '0' + value;
        }
        hours.val(value);
    }
});

$('body').on('click', '.add-minute', function(e) {
    e.preventDefault();
    var minutes = $(this).closest('div').find('input.minutes');
    var value = parseInt(minutes.val()) + 1;
    if(value < 60 && value > -1) {
        if(value < 10) {
            value = '0' + value;
        }
        minutes.val(value);
    }
});

$('body').on('click', '.subtract-minute', function(e) {
    e.preventDefault();
    var minutes = $(this).closest('div').find('input.minutes');
    var value = parseInt(minutes.val()) - 1;
    if(value < 60 && value > -1) {
        if(value < 10) {
            value = '0' + value;
        }
        minutes.val(value);
    }
});